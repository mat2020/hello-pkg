package hello

import (
	"testing"
)

func TestHello(t *testing.T) {
	expected := "Hello, world"
	if ret := Hello(); ret != expected {
		t.Errorf("Hello() = %q, want %q", ret, expected)
	}
}

func TestReverseHello(t *testing.T) {
	expected := "olleH"
	if ret := helloReverse("Hello"); ret != expected {
		t.Errorf("helloReverse() = %q, want %q", ret, expected)
	}
}
