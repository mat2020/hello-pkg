package hello

import (
	"bitbucket.org/calysoft/hello-pkg/morestrings"
)

// Hello bla bla bla
func Hello() string {
	return ("Hello, world v1.1")
}

func helloReverse(s string) string {
	return morestrings.ReverseRunes(s)
}
